import os
import pandas as ps
import matplotlib.pyplot as plt

#Adjust directory and output filename here:
DIR = './BTCEUR15m'
FILE_OUT = 'BTCEUR15m.csv'

FINAL = ""
CONTENTS = ""
FILE_COUNT = 0
LINE_COUNT = 0

for entry in os.scandir(DIR):
    with open(entry.path, 'r') as f:
        temp = f.read().rstrip().splitlines(True)

        del temp[0]
        temp[-1] += "\n"

        FILE_COUNT += 1
        LINE_COUNT += len(temp)
        CONTENTS += ''.join(temp)

with open(FILE_OUT, 'w+') as f:
        FINAL = f"OpenTime,CloseTime,Open,Close,High,Low,Volume,Symbol\n{CONTENTS}"
        f.write(FINAL)

print(f"Merged {FILE_COUNT} files, containing {LINE_COUNT} lines from directory: {DIR}.")

tickerData = ps.read_csv(FILE_OUT)

plt.plot(tickerData["CloseTime"], tickerData["Close"], label=f"{tickerData.iloc[0, -1]} close values")
plt.plot(tickerData["OpenTime"], tickerData["Open"], label=f"{tickerData.iloc[0, -1]} open values")
plt.legend()
plt.show()