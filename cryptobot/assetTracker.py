from log.logger import err
from log import logTypes
from apiManager import accountManager as am
import json


CONFIG = None
with open('./config.json') as f:
    CONFIG = json.load(f)

if CONFIG == None:
    err(logTypes.ERR_NO_CONFIG, " asset tracker couldn't load config")

accInfo = am.getAccountInfo()["balances"]

PairValues = {}
for pairs in CONFIG["pairs"]:
    if pairs["isTracking"] is True:
        PairValues.update({pairs["quote"]: {pairs["symbol"]: {"assetPercentage" : pairs["assetPercentage"], "amountOfBase": 0, "quote": pairs["quote"], "base": pairs["base"]}, "poolSum": 0}})

for currency in accInfo:
    PairValues.update({currency["asset"]: {"poolSum": currency["asset"]["free"]}})


def getQuote(symbol) -> float:
    Quote = 0

    global PairValues
    global accInfo
    global CONFIG

    quote = CONFIG["pairs"][symbol]["quote"]

    accInfo = am.getAccountInfo()["balances"]

    currentBalance = 0
    for balances in accInfo:
        if balances["asset"] == quote:
            currentBalance = balances["asset"]

    Quote = PairValues[quote][symbol]["assetPercentage"] * PairValues[quote]["poolSum"]

    if (currentBalance < Quote):
        err(logTypes.ERR_ASSET_DESYNC, " less funds than calculated quote amount")

    return float(Quote)

def getBase(symbol) -> float:
    return float(PairValues[CONFIG["pairs"][symbol]["quote"]][symbol]["amountOfBase"])

def calculatePool(symbol):
    global PairValues

    quote = CONFIG["pairs"][symbol]["quote"]

    accInfo = am.getAccountInfo()["balances"]
    currentBase = accInfo[quote]["free"]
    amountLocked = 0

    for pools in PairValues:
        if pools != quote:
            for pairs in pools:
                if pairs["base"] == quote:
                    amountLocked += pairs["amountOfBase"]

    PairValues.update({quote: {"poolSum": currentBase - amountLocked}})

def setExecutedQty(symbol, executedQty):
    global PairValues

    quote = CONFIG["pairs"][symbol]["quote"]

    PairValues.update({quote: {symbol: {"amountOfBase": executedQty}}})