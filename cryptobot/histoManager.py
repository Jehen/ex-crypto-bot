import pandas as ps
import converter as cv
from log.logger import log
from log import logTypes

class histoManager:
    def __init__(self, CONFIG):
        self.CONFIG = CONFIG
        self.IMP_DATA = None
        self.COUNTER = 1
        self.oldData = None

    # imports candles from historical data, unless a tickerfile is given and prefered
    def importCandles(self) -> dict:
        if self.CONFIG["dataFile"] != None and self.CONFIG["tickerData"] == False:
            self.IMP_DATA = ps.read_csv("candleData/"+self.CONFIG["dataFile"])
            
            if len(self.IMP_DATA.index) > self.CONFIG["candlesN"]*2:
                self.splitFiles()

            if len(self.IMP_DATA.index) > self.CONFIG["candlesN"]:
                self.IMP_DATA = self.IMP_DATA.iloc[:-self.CONFIG["candlesN"], :]
            
            self.IMP_DATA = self.IMP_DATA.to_dict("records")

            return self.IMP_DATA

        if self.CONFIG["tickerData"] == True and self.CONFIG["tickerFile"] != None:
            return cv.tickerToCandle(self.CONFIG).to_dict("records")
    

    def saveData(self, candleData):
        self.COUNTER += 1

        with open("candleData/"+self.CONFIG["dataFile"], 'a') as fl:
            contents = "\n" + ",".join([str(e) for e in candleData.values()])
            fl.write(contents)

        if self.COUNTER > self.CONFIG["candlesN"]:
            self.IMP_DATA = ps.read_csv("candleData/"+self.CONFIG["dataFile"])

            if len(self.IMP_DATA.index) > self.CONFIG["candlesN"]*2:
                self.splitFiles()


    def splitFiles(self):
        self.oldData = self.IMP_DATA.iloc[:-self.CONFIG["candlesN"], :].reset_index(drop=True)
        self.IMP_DATA = self.IMP_DATA.iloc[-self.CONFIG["candlesN"]:, :].reset_index(drop=True)
        
        genName = str(self.oldData.iloc[0,7])+str(self.oldData.iloc[0,0])
        newHistFileNM = "candleData/historicalData/"+genName+".csv"
        tempData = ""

        self.oldData.to_csv(newHistFileNM, index = False)
        tempData = self.IMP_DATA.to_csv(index = False, line_terminator="\n").rstrip()

        with open("candleData/"+self.CONFIG["dataFile"], 'w') as fl:
            fl.write(tempData)
        
        log(logTypes.EV_FILE_SPLIT, f" name: {genName}")

        self.COUNTER = 1