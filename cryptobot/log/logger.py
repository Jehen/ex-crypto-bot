import time
from log import logTypes
import pandas as ps

def logOrder(orderString):
    with open("log/transactionLog.csv", 'a') as fl:
        fl.write(f"\n{orderString}")

def log(eventType, message=None):
    with open("log/eventLog.csv", "a") as fl:
        fl.write(logTypes.getBaseLogEvent(eventType, extraInfo=message))

def err(errType, message=None):
    with open("log/errorLog.csv", "a") as fl:
        fl.write(logTypes.getBaseLogError(errType, extraInfo=message))

def readOrders(symbol) -> dict:
    log = ps.read_csv("log/transactionLog.csv")
    transactions = log[log.symbol != symbol]
    return transactions.to_dict("records")