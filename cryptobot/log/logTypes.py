import time
import datetime
from orderTracker import order as o

def getTimeMS() -> (int, str):
    t = time.time()
    dt = str(datetime.datetime.fromtimestamp(t))
    t = round(t*1000)
    return t, dt

def getStampToDate(MStimestamp) -> str:
    t = float(MStimestamp)/1000
    return str(datetime.datetime.fromtimestamp(t))

def getBaseLogError(ERR, extraInfo="") -> str:
    t, dt = getTimeMS()
    print(f"\nERROR at:{t} ({dt}), type: {ERR['status']} ({ERR['content']} {extraInfo}), Fatal: {ERR['fatal']}")
    return f"\n{t},{dt},{ERR['status']},{ERR['content']} {extraInfo},{ERR['fatal']}"

def getBaseLogEvent(EV, extraInfo="") -> str:
    t, dt = getTimeMS()
    print(f"\nUPDATE at:{t} ({dt}), type: {EV['status']} ({EV['content']} {extraInfo})")
    return f"\n{t},{dt},{EV['status']},{EV['content']} {extraInfo}"

#------------------------------------------------------------ENUMS:

EV_BOOT = {"status": 10, "content": "Booting bot..."}
EV_FILE_SPLIT = {"status": 11, "content": "Ticker file split"}
EV_STRATFILE_LOAD = {"status": 12, "content": "strat file loaded"}

EV_UNSPECIFIED = {"status": 19, "content": "An unspecified event occurred... extra info:"}

#-----------------------ERR---------------------------------------
ERR_NO_CONFIG = {"status": 1000, "content": "No config was found", "fatal": True}
ERR_NO_IMPORT = {"status": 1001, "content": "An error occurred when trying to import a file", "fatal": True}

ERR_WEBSOCKET_DISCONNECT = {"status": 2000, "content": "Websocket disconnected", "fatal": False}
ERR_WEBSOCKET_NO_RECONNECT = {"status": 2001, "content": "Websocket unable to reconnect... retrying", "fatal": False}
ERR_WEBSOCKET_NO_RECEIVE = {"status": 2002, "content": "Error receiving message from websocket", "fatal": False}
ERR_WEBSOCKET_FATAL = {"status": 2003, "content": "Too many websocket errors occurred", "fatal": True}

ERR_NO_STRATFILE = {"status": 3000, "content": "No strategy file was found", "fatal": True}
ERR_NO_TICKERFILE = {"status": 3001, "content": "No tickerFile was found", "fatal": False}

ERR_API_INTERNAL = {"status": 4000, "content": "An error with our end of the API occurred.", "fatal": False}
ERR_API_EXTERNAL = {"status": 4001, "content": "An error with the Binance API occurred.", "fatal": False}

ERR_ORDER_FALSE_UPDATE = {"status": 5000, "content": "Order update was called... but no value was updated", "fatal": False}
ERR_ORDER_MAX_RETRIES = {"status": 5001, "content": "An order timed out more than the allowed amount (check config)", "fatal": False}
ERR_ORDER_NO_VALUE = {"status": 5002, "content": "No base or quote value was given for the order to execute", "fatal": False}
ERR_ORDER_NO_CANCEL = {"status": 5003, "content": "Order could not be canceled", "fatal": False}

ERR_ASSET_DESYNC = {"status": 6001, "content": "Asset tracker somehow lost track of assets", "fatal": True}

ERR_UNSPECIFIED = {"status": 66, "content": "An unspecified error occurred... extra info:", "fatal": None}