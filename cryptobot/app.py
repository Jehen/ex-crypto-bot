import asyncio
import websockets
import json
import dataHandler as dh
from log.logger import err
from log.logger import log
from log import logTypes

CONFIG = None
DATAHANDLER = None

async def launch(address):
    log(logTypes.EV_BOOT)

    websocket = await websockets.connect(address, ping_interval=None)
    while True:
        if not websocket.open:
            try:
                err(logTypes.ERR_WEBSOCKET_DISCONNECT, f" address: {address}")
                websocket = await websockets.connect(address, ping_interval=None)
            except:
                err(logTypes.ERR_WEBSOCKET_NO_RECONNECT, f" address: {address}")
        try:
            async for message in websocket:
                if message is not None:
                    DATAHANDLER.checkCandle(json.loads(message))
        except:
            err(logTypes.ERR_WEBSOCKET_NO_RECEIVE, f" address: {address}")

if __name__ == "__main__":
    with open('config.json') as f:
        CONFIG = json.load(f)

    if CONFIG != None:
        DATAHANDLER = dh.DataHandler(CONFIG)
        for pairs in CONFIG["pairs"]:
            asyncio.get_event_loop().create_task(launch(CONFIG["baseAddress"]+pairs["address"]+pairs["interval"]))

        asyncio.get_event_loop().run_forever()
    else:
        err(logTypes.ERR_NO_CONFIG, " app.py couldn't import the config.")
