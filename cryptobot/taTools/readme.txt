All TA tools must be made as classes, otherwise they will act as static objects.
All classes should contain an update function, as well as an __init__ function.