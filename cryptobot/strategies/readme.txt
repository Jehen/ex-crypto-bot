All strategy files shouldn't be classes as they only need to track one pair.
Also atm class files cannot be properly dynamically imported.
In order to import them they need to be listed in the config under "stratFile" (without the .py extension).

That aside strat files should contain the necessary TA tool imports.
As well as the functions, buy and sell signal to (later) pass signals to binanceAPI.
And should contain a function update with parameters candleData and currentData.
Update is used by the candle tacker to update strategy file's data.