import pandas as pd
from orderTracker import order
from orderTracker import orderTracker as ot
import taTools.StopLossPlus as spp
import taTools.macd as macd
import taTools.trend as trend
import taTools.volatility as voli
import taTools.volume as vol

SYMBOL = "BTCEUR"
TRACKER = ot.OrderTracker(SYMBOL)
CURPOS = order.TR_SIDE_SELL

#import TaTools
MACD = macd.Macd(5, 10, 9)
TREND = trend.Trend(10, 25, 50)
SPP = spp.StopLossPlus(0.2, 0.2)
VOLITILITY = voli.Volatility(10, 10)
VOLUME = vol.Volume(5, 10)


def buySignal(closeTime, closeValue):
    global CURPOS
    TRACKER.setTransaction(order.Order(symbol=SYMBOL, side=order.TR_SIDE_BUY, value=closeValue, candleTime=closeTime, status=order.TR_ORDER_NEW))
    CURPOS = order.TR_SIDE_BUY

def sellSignal(closeTime, closeValue):
    global CURPOS
    TRACKER.setTransaction(order.Order(symbol=SYMBOL, side=order.TR_SIDE_SELL, value=closeValue, candleTime=closeTime, status=order.TR_ORDER_NEW))
    CURPOS = order.TR_SIDE_SELL

def update(candleData):

    #TODO calculate buy/sell signals
    pass
