from orderTracker import order as o
from log import logTypes
from log.logger import log
from log.logger import err
from orderTracker import orderHandler as oh
from apiManager import orderManager as om
import assetTracker as at
import time
import json

class OrderTracker:
    def __init__(self, symbol):
        self.symbol = symbol
        self.currentSide = o.TR_SIDE_SELL
        self.processing = False

        self.currentOrder = None

        self.lastID = None
        self.followUpCount = 0

        self.config = None
        with open("./config.json") as f:
            self.config = json.load(f)["orderHandling"]

    def getCurrentSide(self) -> str:
        return self.currentSide

    def setTransaction(self, order):
        self.stratSide = order.side
        if order.side != self.currentSide:
            #check if processing an order still cancel order otherwise
            if self.processing == True:
                self.processing = False
                self.cancelOrder(self.currentOrder)
            
            if order.side == o.TR_SIDE_BUY:
                quote = at.getQuote(order.symbol)
                self.trackOrder(order, quote=quote)
            else:
                base = at.getBase(order.symbol)
                self.trackOrder(order, base=base)

    def trackOrder(self, order, base=0, quote=0):
        self.processing = True
        self.currentOrder = order
        res = None

        if base > 0 or quote > 0:
            if base > 0:
                res = om.newOrder(self.symbol, order.side, base, order.orderID)
            else:
                res = om.newOrder(self.symbol, order.side, quote, order.orderID)
            
            if res["status"] != 200:
                err(logTypes.ERR_API_EXTERNAL, res["content"])
                return

            order.update(status=res["content"]["status"], executedQty=res["content"]["executedQty"])
            order.logself()

            while res["content"]["status"] != o.TR_ORDER_FILLED:
                if self.processing != True:
                    self.cancelOrder(order)
                    return
                
                time.sleep(self.config["pingTime"])
                res = om.getOrder(self.symbol, order.orderID)

                if res["content"]["status"] != order.status:
                    order.update(status=res["content"]["status"], executedQty=res["content"]["executedQty"])
                    order.logself()
                
                if res["content"]["status"] == o.TR_ORDER_EXPIRED:
                    self.followUp(order)
                    return
        else:
            err(logTypes.ERR_ORDER_NO_VALUE, f" id: {order.orderID} symbol: {self.symbol}")

        self.processing = False
        self.currentSide = order.side

        if order.side == o.TR_SIDE_BUY:
            at.setExecutedQty(order.symbol, order.executedQty)
        else:
            at.calculatePool(order.symbol)

    def cancelOrder(self, order):
        res = om.cancelOrder(self.symbol, orderID=order.orderID)
        if res["content"]["status"] != o.TR_ORDER_CANCELED:
            err(logTypes.ERR_ORDER_NO_CANCEL, f" symbol: {self.symbol}... id: {order.orderID}")

        if res["content"]["executedQty"] > 0:
            if order.side == o.TR_SIDE_BUY:
                at.setExecutedQty(self.symbol, res["content"]["executedQty"])
            else:
                at.calculatePool(self.symbol)
        
        self.processing = False
        
        if order.side == o.TR_SIDE_BUY:
            self.currentSide = o.TR_SIDE_SELL
        else:
            self.currentSide = o.TR_SIDE_BUY
    
    def followUp(self, order):
        if self.processing != True:
            self.cancelOrder(order)
            return

        if self.followUpCount > self.config["retries"]:
            err(logTypes.ERR_ORDER_MAX_RETRIES, f" symbol: {self.symbol}... id: {order.orderID}")
            order.update(status=o.TR_ORDER_EXPIRED)
            order.logself()
            self.processing = False
            self.followUpCount = 0
            return

        if self.lastID == order.orderID:
            self.followUpCount += 1
        else:
            self.followUpCount = 0
        
        newOrd = order.toDict()
        newOrd["status"] = o.TR_ORDER_NEW
        newOrd["timestamp"] = None
        newOrd["date"] =  None
        newOrd["orderID"] = None
        newOrd["executedQty"] = None
        newOrd = oh.orderFromDict(newOrd)

        self.lastID = newOrd.orderID
        
        if newOrd.side == o.TR_SIDE_BUY:
            quote = at.getQuote(newOrd.symbol)
            self.trackOrder(newOrd, quote=quote)
        else:
            base = at.getBase(newOrd.symbol)
            self.trackOrder(newOrd, base=base)
