import time
from log.logTypes import getTimeMS as getTime
from log.logTypes import getStampToDate as getDate
import log.logTypes as lt
import uuid
from log import logger

class Order:
    def __init__(self, symbol, side, value, candleTime, status, executedQty=None, orderID=None, timestamp=None):
        if timestamp is None:
            self.timestamp, self.date = getTime()
        else:
            self.timestamp = int(timestamp)
            self.date = getDate(timestamp)

        if orderID is None:
            orderID = str(uuid.uuid4())
        self.orderID = str(orderID)

        self.symbol = symbol
        self.side = side
        self.value = float(value)
        self.candleTime = int(candleTime)
        self.status = status
        self.executedQty = executedQty
    
    def update(self, status=None, executedQty=None):
        if status != None:
            self.status = status
            self.timestamp, self.date = getTime()
        elif executedQty != None:
            self.executedQty = executedQty
            self.timestamp, self.date = getTime()
        else:
            logger.log(lt.ERR_ORDER_FALSE_UPDATE)
    
    def logself(self):
        logger.logOrder(self.toStr())

    def toDict(self) -> dict:
        return {
            "timestamp": self.timestamp,
            "date": self.date,
            "orderID": self.orderID,
            "symbol": self.symbol,
            "side": self.side,
            "value": self.value,
            "candleTime": self.candleTime,
            "executedQty": self.executedQty,
            "status": self.status
        }
    
    def toStr(self) -> str:
        return f"{self.timestamp},{self.date},{self.orderID},{self.symbol},{self.side},{self.value},{self.executedQty},{self.candleTime},{self.status}"

#==============================================[ENUMS]:
TR_SIDE_BUY = "BUY"
TR_SIDE_SELL = "SELL"

#order statuses
TR_ORDER_NEW = "NEW"
TR_ORDER_ACK = "ACK"
TR_ORDER_FILLED = "FILLED"
TR_ORDER_CANCEL = "CANCEL"
TR_ORDER_CANCELED = "CANCELED"
TR_ORDER_EXPIRED = "EXPIRED"