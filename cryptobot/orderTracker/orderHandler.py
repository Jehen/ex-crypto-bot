from orderTracker.order import Order

def orderFromDict(order) -> Order:
    return Order(order["symbol"], order["side"], order["value"], order["candleTime"], order["status"], order["executedQty"], order["orderID"], order["timestamp"])

def orderFromStr(orderString) -> Order:
    ol = orderString.split(",")
    return Order(ol[3], ol[4], ol[5], ol[7], ol[8], ol[6], ol[2], ol[0])