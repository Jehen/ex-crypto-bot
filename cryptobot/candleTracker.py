import importlib
import strategies
import histoManager
import pandas as ps
from log.logger import log
from log.logger import err
from log import logTypes

class CandleTracker:
    def __init__(self, CONFIG):
        self.CONFIG = CONFIG
        self.symbol = CONFIG["symbol"]
        self.CANDLEn = []
        self.CURRENTVALUES = None
        self.stratFile = None
        self.histo = histoManager.histoManager(self.CONFIG)

        if CONFIG["stratFile"] != None:
            self.stratFile = importlib.import_module("strategies."+CONFIG["stratFile"])
            self.stratFile.init()
            log(logTypes.EV_STRATFILE_LOAD, f" symbol: {self.symbol}")
        else:
            err(logTypes.ERR_NO_STRATFILE, f" symbol: {self.symbol}")

        self.CANDLEn += self.histo.importCandles()

    def saveCandle(self, candle):
        self.CANDLEn.append(candle)
        if len(self.CANDLEn) > self.CONFIG["candlesN"]:
            del self.CANDLEn[0]
        self.updateStratFile()

    def saveCurrent(self, currentData):
        self.CURRENTVALUES = currentData
        self.updateStratFile()
    
    def updateStratFile(self):
        if self.stratFile != None:
            allCandles = []
            allCandles.append(self.CANDLEn)
            allCandles.append(self.CURRENTVALUES)
            self.stratFile.update(ps.DataFrame(data=allCandles))