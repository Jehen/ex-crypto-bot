import pandas as ps
from log.logger import err
from log import logTypes

CONFIG = None

def tickerToCandle(config):
    CONFIG = config

    if CONFIG["tickerFile"] != None:
        td = ps.read_csv("tickerData/"+CONFIG["tickerFile"])
        td = td.drop_duplicates(subset=['OpenTime'], keep='last').reset_index(drop=True)
        return td
    
    err(logTypes.ERR_NO_CONFIG, " converter wasn't given a config")