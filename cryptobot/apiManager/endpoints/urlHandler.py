import json
import time
import hmac
import hashlib
from log.logger import err
from log import logTypes


API_CONFIG = None

with open('./config.json') as f:
    API_CONFIG = json.load(f)["api"]

def sign(query):
    return hmac.new(API_CONFIG["keys"]["secret"].encode('utf-8'), query.encode('utf-8'), hashlib.sha256).hexdigest()

def getURL(endpoint=None, queryParams=None) -> (str, str):
    if endpoint == None:
        err(logTypes.ERR_API_INTERNAL, " no endpoint received when trying to formulate URL")
        return str(logTypes.ERR_API_INTERNAL["status"]), logTypes.ERR_API_INTERNAL["content"]

    url = API_CONFIG["baseAddress"]
    url += API_CONFIG["urls"][endpoint]

    if API_CONFIG["placeholders"]["time"] in url:
        url = url.replace(API_CONFIG["placeholders"]["time"], str(round(time.time()*1000)))

    if queryParams is not None:
        for param in queryParams:
            if queryParams[param] is not None:
                url = url.replace(API_CONFIG["placeholders"][param], queryParams[param])
    
    if API_CONFIG["placeholders"]["signature"] in url:
        url = url.replace(API_CONFIG["placeholders"]["signature"], sign(url[url.index("?")+1:].replace(API_CONFIG["placeholders"]["signatureExclude"], "")))

    return url, API_CONFIG["keys"]["apikey"]

def getBaseParams(symbol=None, buysell=None, quantity=None, orderID=None, orderNr=None) -> dict:
    return {
        "symbol": symbol,
        "buysell": buysell,
        "quantity": str(quantity),
        "orderID": str(orderID),
        "orderNr": str(orderNr)
    }