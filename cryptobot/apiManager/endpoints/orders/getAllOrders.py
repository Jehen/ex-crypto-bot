from apiManager.endpoints import urlHandler
import aiohttp
import json

async def getAll(symbol, orderNr) -> dict:
    params = urlHandler.getBaseParams(symbol=symbol, orderNr=orderNr)

    url, key = urlHandler.getURL("orderOverview", params)

    async with aiohttp.ClientSession() as s:
        async with s.get(url, headers={"X-MBX-APIKEY": key}) as res:
            result = {"status": res.status, "content": json.loads(await res.read())}
            return result