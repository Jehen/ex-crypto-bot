from apiManager.endpoints import urlHandler
import aiohttp
import json

async def place(symbol, buysell, quantity, orderID) -> dict:
    params = urlHandler.getBaseParams(symbol=symbol, buysell=buysell, quantity=quantity, orderID=orderID)

    url, key = urlHandler.getURL("newOrder", params)

    async with aiohttp.ClientSession() as s:
        async with s.post(url, headers={"X-MBX-APIKEY": key}) as res:
            result = {"status": res.status, "content": json.loads(await res.read())}
            return result