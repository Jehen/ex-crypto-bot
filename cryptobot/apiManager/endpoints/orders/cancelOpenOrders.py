from apiManager.endpoints import urlHandler
import aiohttp
import json

async def cancelOpen(symbol, orderID) -> dict:
    params = urlHandler.getBaseParams(symbol=symbol)

    url, key = urlHandler.getURL("cancelOpenOrders", params)

    async with aiohttp.ClientSession() as s:
        async with s.delete(url, headers={"X-MBX-APIKEY": key}) as res:
            result = {"status": res.status, "content": json.loads(await res.read())}
            return result