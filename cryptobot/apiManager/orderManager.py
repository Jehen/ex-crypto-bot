from apiManager.endpoints.orders import newOrder as no
from apiManager.endpoints.orders import getAllOrders as gao
from apiManager.endpoints.orders import getOrder as go
from apiManager.endpoints.orders import cancelOrder as co
from apiManager.endpoints.orders import getOpenOrders as goo
import asyncio
from log import logTypes
from log.logger import err

def newOrder(symbol, buysell, quantity, orderID) -> dict:
    try:
        return asyncio.run(no.place(symbol, buysell, quantity, orderID))
    except:
        err(logTypes.ERR_API_INTERNAL)
        return logTypes.ERR_API_INTERNAL

def getAllOrders(symbol, orderNr) -> dict:
    try:
        return asyncio.run(gao.getAll(symbol, orderNr))
    except:
        err(logTypes.ERR_API_INTERNAL)
        return logTypes.ERR_API_INTERNAL

def getOrder(symbol, orderID) -> dict:
    try:
        return asyncio.run(go.get(symbol, orderID))
    except:
        err(logTypes.ERR_API_INTERNAL)
        return logTypes.ERR_API_INTERNAL

def cancelOrder(symbol, orderID) -> dict:
    try:
        return asyncio.run(co.cancel(symbol, orderID))
    except:
        err(logTypes.ERR_API_INTERNAL)
        return logTypes.ERR_API_INTERNAL

def getOpenOrders(symbol) -> dict:
    try:
        return asyncio.run(goo.getAllOpen(symbol))
    except:
        err(logTypes.ERR_API_INTERNAL)
        return logTypes.ERR_API_INTERNAL