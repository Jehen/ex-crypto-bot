from apiManager.endpoints.account import accountInfo as aci
import asyncio
from log import logTypes
from log.logger import err

def getAccountInfo() -> dict:
    try:
        return asyncio.run(aci.get())
    except:
        err(logTypes.ERR_API_INTERNAL)
        return logTypes.ERR_API_INTERNAL
    