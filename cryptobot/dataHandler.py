import candleTracker
import histoManager

class DataHandler:
    def __init__(self, CONFIG):
        self.CONFIG = CONFIG
        self.TRACKERS = {}
        self.CANDLE_TYPES = {}
        self.HISTOS = {}

        for pairs in self.CONFIG["pairs"]:
            self.TRACKERS.update({pairs["symbol"]: candleTracker.CandleTracker(pairs)})
            self.CANDLE_TYPES.update({pairs["symbol"]: self.get_BASE_CANDLE()})
            self.HISTOS.update({pairs["symbol"]: histoManager.histoManager(pairs)})

    def get_BASE_CANDLE(self) -> dict:
            BASE_CANDLE = {
                "OpenTime": None,
                "CloseTime": None,
                "Open": None,
                "Close": None,
                "High": None,
                "Low": None,
                "Volume": None,
                "Symbol": None
            }
            return BASE_CANDLE        


    def fillBase(self, candleData, symbol):
            self.CANDLE_TYPES[symbol]["OpenTime"] = candleData["k"]["t"]
            self.CANDLE_TYPES[symbol]["CloseTime"] = candleData["k"]["T"]
            self.CANDLE_TYPES[symbol]["Open"] = float(candleData["k"]["o"])
            self.CANDLE_TYPES[symbol]["Close"] = float(candleData["k"]["c"])
            self.CANDLE_TYPES[symbol]["High"] = float(candleData["k"]["h"])
            self.CANDLE_TYPES[symbol]["Low"] = float(candleData["k"]["l"])
            self.CANDLE_TYPES[symbol]["Volume"] = float(candleData["k"]["v"])
            self.CANDLE_TYPES[symbol]["Symbol"] = symbol

            #still not sure if volume is self aggregating or not
            #if self.CANDLE_TYPES[symbol]["Volume"] != None:
            #    self.CANDLE_TYPES[symbol]["Volume"] += float(candleData["k"]["v"])
            #else:
            #    self.CANDLE_TYPES[symbol]["Volume"] = float(candleData["k"]["v"])

    def checkCandle(self, candleData):
        symbol = candleData["k"]["s"]
        print(f"{symbol} data received")

        #init first received candle
        if self.CANDLE_TYPES[symbol]["OpenTime"] == None or self.CANDLE_TYPES[symbol]["OpenTime"] == candleData["k"]["t"]:
            self.fillBase(candleData, symbol)
        
            self.TRACKERS[symbol].saveCurrent(self.CANDLE_TYPES[symbol])

        #if a new candle is passed close the old one and write to .csv
        if self.CANDLE_TYPES[symbol]["OpenTime"] < candleData["k"]["t"]:
            print(f"{symbol} candle closed, new one opened.")

            self.CANDLE_TYPES[symbol] = self.get_BASE_CANDLE()
            self.fillBase(candleData, symbol)

            self.HISTOS[symbol].saveData(self.CANDLE_TYPES[symbol])
            
            self.TRACKERS[symbol].saveCandle(self.CANDLE_TYPES[symbol])