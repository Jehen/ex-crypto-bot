import time
import datetime

def getTimeMS() -> (int, str):
    t = time.time()
    dt = str(datetime.datetime.fromtimestamp(t))
    t = round(t*1000)
    return t, dt

def getStampToDate(MStimestamp) -> str:
    t = float(MStimestamp)/1000
    return str(datetime.datetime.fromtimestamp(t))
