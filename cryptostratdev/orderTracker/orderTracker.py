from orderTracker import order as o
import logger
import json

FIN_VAL = 0
GREEN_ORDERS = []
YELLOW_ORDERS = []
RED_ORDERS = []
DIP_ORDERS = []
ALL_ORDERS = []

class OrderTracker:
    def __init__(self, symbol):
        self.symbol = symbol

        self.PREV_ORDER = None

        self.CONFIG = None
        with open("config.json") as f:
            self.CONFIG = json.load(f)

        if self.CONFIG != None:
            self.START_VAL = self.CONFIG["startVal"]
            self.COMMISSION = self.CONFIG["commission"]/100
        else:
            self.START_VAL = 100
            self.COMMISSION = 0.2/100

        self.CUR_VAL = self.START_VAL


    def setTransaction(self, order):
        logger.setTransactions(order.toDict())
        self.calcDiff(order)


    def calcDiff(self, order):
        percent = 0
        logPct = ""
        fee = self.CUR_VAL*self.COMMISSION

        if self.PREV_ORDER != None:
            percent = ((100/self.PREV_ORDER.value)*order.value)-100
            factor = percent/100

            if order.side == o.TR_SIDE_SELL:
                self.CUR_VAL = (self.CUR_VAL * (1+factor)) - fee
            else:
                self.CUR_VAL = self.CUR_VAL - fee
        else:
            self.CUR_VAL = self.CUR_VAL - fee

        logPct = f"{percent:.2f}"

        if order.side == o.TR_SIDE_BUY:
            if percent > 0:
                #print yellow - we missed a growth in value
                global YELLOW_ORDERS
                YELLOW_ORDERS.append(percent)
                percent = f"\033[93m+{percent:.2f}\033[00m"
            else:
                global DIP_ORDERS
                DIP_ORDERS.append(percent)
                #print green - we bought at a lower price than before
                percent = f"\033[92m{percent:.2f}\033[00m"
        else:
            if percent > 0:
                global GREEN_ORDERS
                GREEN_ORDERS.append(percent)
                #print green - we sold at a higher value
                percent = f"\033[92m+{percent:.2f}\033[00m"
            else:
                global RED_ORDERS
                RED_ORDERS.append(percent)
                #print red - we sold at a lower value
                percent = f"\033[91m{percent:.2f}\033[00m"


        if order.side == o.TR_SIDE_BUY:
            print(f"Bought at:   {order.value:.2f},    delta %: {percent}%,    quote qty: {self.CUR_VAL:.2f},    fee: {fee:.3f}")
            ALL_ORDERS.append(f"Bought at:   {order.value:.2f},    delta %: {logPct}%,    quote qty: {self.CUR_VAL:.2f},    fee: {fee:.3f}\n")
        else:
            print(f"Sold at:     {order.value:.2f},    delta %: {percent}%,    quote qty: {self.CUR_VAL:.2f},    fee: {fee:.3f}")
            ALL_ORDERS.append(f"Sold at:     {order.value:.2f},    delta %: {logPct}%,    quote qty: {self.CUR_VAL:.2f},    fee: {fee:.3f}\n")

        self.PREV_ORDER = order

        global FIN_VAL
        FIN_VAL = self.CUR_VAL