import pandas as ps
import json
import importlib
import logger
import time

CONFIG = None
TICKER_DATA = None
CANDLES = []
STRATFILE = None

if __name__ == "__main__":
    start = time.time()

    with open('config.json') as f:
        CONFIG = json.load(f)
    
    taCandles = []

    if CONFIG['tickerFile'] != None and CONFIG['stratFile'] != None:
        TICKER_DATA = ps.read_csv(f"tickerFiles/{CONFIG['tickerFile']}").to_dict("records")
        STRATFILE = importlib.import_module("stratFiles."+CONFIG["stratFile"])

        for i, _ in enumerate(TICKER_DATA):
            if TICKER_DATA[i]["OpenTime"] > TICKER_DATA[i-1]["OpenTime"]:
                CANDLES.append(TICKER_DATA[i-1])
                taCandles.append(TICKER_DATA[i-1])

            if len(taCandles) > CONFIG["candlesN"]:
                del taCandles[0]

            STRATFILE.update(ps.DataFrame(data=taCandles))
        
        logger.setCandles(CANDLES)
        logger.simulate(startTime=start)

    else:
        print("ticker and strategy files could not be found!")
