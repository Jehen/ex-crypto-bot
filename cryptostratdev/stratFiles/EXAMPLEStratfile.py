import time
import pandas as pd
from orderTracker import order
from orderTracker import orderTracker as ot
import taTools.stopLossPlus as spp
import taTools.macd as macd
import taTools.trend as trend
import taTools.volatility as voli
import taTools.volume as vol
import taTools.priceOscilator as po

SYMBOL = "EXAMPLEPAIR"
TRACKER = ot.OrderTracker(SYMBOL)
CURPOS = order.TR_SIDE_SELL

#import TaTools
#ommitted 

BUY_SIGN_COUNT = 0
SELL_SIGN_COUNT = 0


def buySignal(closeTime, closeValue):
    global CURPOS
    TRACKER.setTransaction(order.Order(symbol=SYMBOL, side=order.TR_SIDE_BUY, value=closeValue, candleTime=closeTime, status=order.TR_ORDER_NEW))
    CURPOS = order.TR_SIDE_BUY

def sellSignal(closeTime, closeValue):
    global CURPOS
    TRACKER.setTransaction(order.Order(symbol=SYMBOL, side=order.TR_SIDE_SELL, value=closeValue, candleTime=closeTime, status=order.TR_ORDER_NEW))
    CURPOS = order.TR_SIDE_SELL

def update(candleData):
    global BUY_SIGN_COUNT
    global SELL_SIGN_COUNT

    #Buy condition logic
    #ommitted
    #   BUY_SIGN_COUNT += 1
    #   SELL_SIGN_COUNT = 0
    
    #Sell condition logic
    #omitted
    #   SELL_SIGN_COUNT += 1
    #   BUY_SIGN_COUNT = 0
    

    if SELL_SIGN_COUNT > 75:
        sellSignal(candleData.iloc[-1]['CloseTime'], candleData.iloc[-1]['close'])
        SELL_SIGN_COUNT = 0
    
    if BUY_SIGN_COUNT > 75:
        buySignal(candleData.iloc[-1]['CloseTime'], candleData.iloc[-1]['close'])
        BUY_SIGN_COUNT = 0
