import json
from pprint import pp
import matplotlib.pyplot as plt
import math
import pandas as pd
import orderTracker.order as o
import orderTracker.orderTracker as ot
import time

#TA tools to render
from taTools import macd
from taTools import volume

CONFIG = None
with open('config.json') as f:
    CONFIG = json.load(f)

CANDLES = []
TRANSACTIONS = []

def setCandles(candleData):
    global CANDLES
    CANDLES = candleData

def setTransactions(transactions):
    global TRANSACTIONS
    TRANSACTIONS.append(transactions)

def formatPosNeg(n) -> float:
    temp = ""
    if n > 0:
        temp = f"+{n}"
        return float(temp)
    else:
        return n

def finalCalc() -> (float, float, int):
    initVal = CONFIG["startVal"]
    finalVal = ot.FIN_VAL

    deltaPercent = 100/initVal*finalVal-100
    deltaValue = finalVal-initVal
    deltaPeriod = len(CANDLES)

    deltaPercent = formatPosNeg(deltaPercent)
    deltaValue = formatPosNeg(deltaValue)

    return deltaPercent, deltaValue, deltaPeriod

def colourFinalStats(n) -> str:
    if n > 0:
        return f"\033[92m+{n:.5f}\033[00m"
    else:
        return f"\033[91m-{n:.5f}\033[00m"

def simulate(startTime=None):
    toPrint = ""
    toLog = ""

    CDL = pd.DataFrame(data=CANDLES)
    TRN = pd.DataFrame(data=TRANSACTIONS)
    
    BuySig = pd.DataFrame()
    SellSig = pd.DataFrame()

    for i in range(len(TRN)):
        if TRN["side"][i] == o.TR_SIDE_BUY:
            BuySig = BuySig.append(TRN.iloc[i])
        else:
            SellSig = SellSig.append(TRN.iloc[i])

    pct, val, per = finalCalc()
    
    try:
        nGreen = len(ot.GREEN_ORDERS)
        avgGreen = sum(ot.GREEN_ORDERS)/len(ot.GREEN_ORDERS)
        nRed = len(ot.RED_ORDERS)
        avgRed = sum(ot.RED_ORDERS)/len(ot.RED_ORDERS)
        nYellow = len(ot.YELLOW_ORDERS)
        avgYellow = sum(ot.YELLOW_ORDERS)/len(ot.YELLOW_ORDERS)
        nDip = len(ot.DIP_ORDERS)
        avgDip = sum(ot.DIP_ORDERS)/len(ot.DIP_ORDERS)

        toPrint =           f"\n=================== TOTAL DIFFERENCE ===================\n"
        toLog =             f"\n=================== TOTAL DIFFERENCE ===================\n"
        toPrint +=          f"Delta %:       {colourFinalStats(pct)}%\n"
        toPrint +=          f"Delta quote:   {colourFinalStats(val)}\n"
        toLog +=            f"Delta %:       {pct:.5f}%\n"
        toLog +=            f"Delta quote:   {val:.5f}\n"
        toPrint +=          f"Delta periods: {per} candles\n"
        toPrint +=          f"stratfile:     {CONFIG['stratFile']}\n"
        toPrint +=          f"tickerfile:    {CONFIG['tickerFile']}\n\n"
        toLog +=            f"Delta periods: {per} candles\n"
        toLog +=            f"stratfile:     {CONFIG['stratFile']}\n"
        toLog +=            f"tickerfile:    {CONFIG['tickerFile']}\n\n"

        toPrint += f"\033[92mGreen\033[00m sell orders:               \033[92m{nGreen}\033[00m, average profit:            \033[92m+{avgGreen:.5f}\033[00m%\n"
        toPrint += f"\033[91mRed\033[00m sell orders:                 \033[91m{nRed}\033[00m, average losses:            \033[91m{avgRed:.5f}\033[00m%\n"
        toPrint += f"\033[93mMissed potential\033[00m buy orders:     \033[93m{nYellow}\033[00m, average potential missed:  \033[93m+{avgYellow:.5f}\033[00m%\n"
        toPrint += f"\033[92mIncreased potential\033[00m buys orders: \033[92m{nDip}\033[00m, average potential gained:  \033[92m{avgDip:.5f}\033[00m%\n"

        toLog += f"Green sell orders:               {nGreen}, average profit:            +{avgGreen:.5f}%\n"
        toLog += f"Red sell orders:                 {nRed}, average losses:            {avgRed:.5f}%\n"
        toLog += f"Missed potential buy orders:     {nYellow}, average potential missed:  +{avgYellow:.5f}%\n"
        toLog += f"Increased potential buys orders: {nDip}, average potential gained:  {avgDip:.5f}%\n"  
    except:
        toPrint = "\033[91mINSUFFICIENT ORDERS WERE PRODUCED TO CALCULATE STATISTICAL DATA\033[00m\n"
        toLog = "INSUFFICIENT ORDERS WERE PRODUCED TO CALCULATE STATISTICAL DATA\n"

    if startTime != None:
        end = time.time()

        sec = (end-startTime)%60

        toPrint += f"\nSim time:      {(end-startTime)/60:.0f}m {sec:.0f}s\n"
        toLog += f"\nSim time:      {(end-startTime)/60:.0f}m {sec:.0f}s\n"
    
    print(toPrint)

    toLog += "\n=================== COMPLETE ORDER LOG ===================\n"
    for el in ot.ALL_ORDERS:
        toLog += el
    
    with open(f"simLogs/log_{CONFIG['tickerFile']}_{CONFIG['stratFile']}_{pct:.2f}%_{str(round(1000*time.time()))}.txt", "w+") as f:
        f.write(toLog)

    #CALCULATE IF NECESSARY the displays of TA tools ==============================================
    
    #MACD = macd.Macd(12, 30, 9)
    #VOL = volume.Volume(5, 10)

    #==============================================================================================


    plt.plot(CDL["CloseTime"], CDL["Close"], label=f"{CDL.iloc[0,-1]} close values")
    try:
        plt.plot(BuySig["candleTime"], BuySig["value"], 'g^', label="buy")
        plt.plot(SellSig["candleTime"], SellSig["value"], 'rv', label="sell")
    except:
        print("\033[91mNO BUY AND/OR SELL ORDERS WERE GENERATED\033[00m")
    plt.legend()
    plt.show()

