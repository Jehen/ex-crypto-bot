# Overview
This project is a simple showcase of a crypto trading bot for the Binance API.
This is a clone from a private staging repo from which we removed all sensitive data/code.
You may do with this code what you want, it is more than likely to change in the future.
This project is put out as a proof of concept/part of portfolio/example that can be shared and showed easily.

# Censored
Some of the code has been removed because obviously we don't want to share our trade secrets.

Items that have been removed include:
- sim logs                   (These contain the results of simulations ran against certain strategies)
- ta tools                   (Despite these tools being based on pre-existing TA-tools we have nontheless made custom variants)
- venvs                      (python virtual environments)
- API keys                   (Should be obvious)
- strategy files             (These contain trade trategies and thus trade secrets)

Any other sensitive code, which has been replaced with "ommitted".

Needless to say the included 'programs' don't run.
Except the 'processed results' which works if you have matplotlib & pandas installed.

# Context
- This is still an alpha version.
- Much refactoring still has to be done, but currently we are in the prototyping phase.
- We use several different python virtual environments.
- ASYNC is not always used everywhere because our dataflow needs to be linear, because every input needs to be carefully and accurately calculated one at a time as the last result affects the next.
And ASYNC is (in python) a lot faster than not especially with API's and websockets. 


## Cryptobot
We receive information about selected trading pairs (e.g. BTC & BNB) several times per second.
This data is saved to a .csv file and every so often the file is split (splitting interval is configurable via config).
We do not use a database because a database is too slow to keep up with multiple trading pairs each updating multiple times per second, especially on the long run.
And finally we format that data so that we can use it in our algorithm which can then in return make Binance API calls.


## cryptostratdev
This is a custom built development environment for developing trading strategies, based on historical ticker data.
It also logs all transactions, draws a graph afterward with all the data and logs losses, profits and potentials.


## processedResults
This is a simple tool that automatically mergers all given ticker files.
Manually merging half a dozen files isn't hard, but merging 20, 30 or dozens more is not only horrible and slow but also error prone.
Once done this script also draws a graph so you can see if it looks like it should and so you can catch any weird entries.
